import { DatePipe } from '@angular/common';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-date-range',
  templateUrl: './date-range.component.html',
  styleUrls: ['./date-range.component.scss'],
})
export class DateRangeComponent implements OnInit {

  public fromStringDate: string;
  public toStringDate: string;

  @Output() fromDateInMsChanged: EventEmitter<number> = new EventEmitter<number>();
  @Output() toDateInMsChanged: EventEmitter<number> = new EventEmitter<number>();

  constructor(private datePipe: DatePipe) { }

  ngOnInit() {
    this.fromDateInMsChanged.subscribe(date => {
      this.fromStringDate = this.datePipe.transform(date, "yyyy-MM-dd", "+02", "en-GB");
    });
    this.toDateInMsChanged.subscribe(date => {
      this.toStringDate = this.datePipe.transform(date, "yyyy-MM-dd", "+02", "en-GB");
    });
    let date: Date = new Date(Date.now());
    date.setHours(0, 0, 0, 0);
    this.fromDateInMsChanged.emit(date.getTime());
    date.setHours(23, 59, 59, 999);
    this.toDateInMsChanged.emit(date.getTime());
  }

  public fromDateChanged(dateEvent: CustomEvent): void {
    let date: Date = new Date(dateEvent.detail.value);
    date.setHours(0, 0, 0, 0);
    this.fromDateInMsChanged.emit(date.getTime());
  }

  public toDateChanged(dateEvent: CustomEvent): void {
    let date: Date = new Date(dateEvent.detail.value);
    date.setHours(23, 59, 59, 999);
    this.toDateInMsChanged.emit(date.getTime());
  }
}
