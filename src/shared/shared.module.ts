import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateRangeComponent } from './date-range-component/date-range.component';



@NgModule({
  declarations: [DateRangeComponent],
  imports: [
    CommonModule
  ],
  exports: [DateRangeComponent]
})
export class SharedModule { }
