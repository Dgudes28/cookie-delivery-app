import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of, Subject } from 'rxjs';
import { AuthService } from 'src/core/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  isLoggedInSubject: Subject<boolean> = new Subject();
  isLoggedIn$: Observable<boolean> = this.isLoggedInSubject.asObservable();
  /**
   *
   */
  constructor(private authService: AuthService, private router: Router, private auth: AngularFireAuth) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    this.auth.onAuthStateChanged(user => {
      if (user) {
        this.authService.isLoggedIn = true;
        this.isLoggedInSubject.next(true);
      } else {
        this.authService.isLoggedIn = false;
        this.router.navigateByUrl('tabs/sign-in');
        this.isLoggedInSubject.next(false);
      }
    });
    return this.isLoggedIn$;
    // if (!this.authService.isLoggedIn) {
    //   this.auth.setPersistence('local');
    //   debugger
    //   this.router.navigateByUrl('tabs/sign-in');
    //   return false;
  }
  // return true;
}
