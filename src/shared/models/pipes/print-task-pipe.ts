import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import { TaskWithId } from '../taskWithId';

@Pipe({ name: 'printTask' })
export class PrintTaskPipe implements PipeTransform {
    /**
     *
     */
    constructor(private datePipe: DatePipe) {

    }
    transform(task: TaskWithId): string {
        return `${task.task.adress} ,${this.datePipe.transform(task.task.date, "dd-MM-yyyy")}, ${task.task.time}, ₪${task.task.price}`;
    }
}