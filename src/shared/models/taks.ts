export class Task {
    date: string;
    time: string;
    adress: string;
    isCompleted: boolean;
    price: number;
    content: string;
    /**
     *
     */
    constructor(date: string, time: string, adress: string, content: string = "", price: number = 0, isCompletd = false,) {
        this.date = date;
        this.time = time;
        this.adress = adress;
        this.price = price;
        this.isCompleted = isCompletd;
        this.content = content;
    }
}