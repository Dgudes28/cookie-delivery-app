import { Task } from './taks';

export class TaskWithId {
    id: string;
    task: Task;

    constructor(id: string, task: Task) {
        this.task = task;
        this.id = id;
    }
}