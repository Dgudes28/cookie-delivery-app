import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TaskWithId } from 'src/shared/models/taskWithId';

@Component({
  selector: 'app-stats-view',
  templateUrl: './stats-view.component.html',
  styleUrls: ['./stats-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StatsViewComponent implements OnInit {

  @Input() title: string;
  @Input() values$: Observable<TaskWithId[]>;
  @Input() price$: Observable<number>;
  public price: number;
  constructor() { }

  ngOnInit() {
    this.price$.subscribe(price => this.price = price);
  }

}
