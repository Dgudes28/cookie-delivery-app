import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ApiService } from 'src/core/api-service';
import { TaskWithId } from 'src/shared/models/taskWithId';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-daily-stats',
  templateUrl: './daily-stats.component.html',
  styleUrls: ['./daily-stats.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DailyStatsComponent implements OnInit {

  @Input() fromDate$: Observable<number>;
  @Input() toDate$: Observable<number>;

  currentFromDate: number;
  currentToDate: number;

  @Input() isCompleted: boolean;
  @Input() title: string;
  tasksSubject: BehaviorSubject<TaskWithId[]> = new BehaviorSubject<TaskWithId[]>([]);
  tasks$: Observable<TaskWithId[]> = this.tasksSubject.asObservable();
  price$: Observable<number> = this.tasks$.pipe(map(data => this.calculateSumPriceFromArray(data)));
  constructor(private apiService: ApiService, private cd: ChangeDetectorRef) { }

  ngOnInit() {
    this.apiService.getTasksValues().subscribe((tasks: TaskWithId[]) => {
      this.tasksSubject.next(tasks.filter(task => this.filterArrayByCompletedTasksOrNot(task, this.isCompleted)));
      this.price$ = this.createSumPriceObservable();
      this.cd.detectChanges();
    });
    this.fromDate$.subscribe((date: number) => {
      // console.log("daily stats changed", date);
      this.currentFromDate = date;
      this.updateTasksCounterAndPrice();
    });
    this.toDate$.subscribe((date: number) => {
      this.currentToDate = date;
      this.updateTasksCounterAndPrice();
    });
  }

  private calculateSumPriceFromArray(tasks: TaskWithId[]): number {
    let numOr0 = n => isNaN(n) ? 0 : n;

    if (tasks.length > 0) {
      return tasks.map(task => task.task.price).reduce((a, b) =>
        parseInt(numOr0(a)) + parseInt(numOr0(b)));
    }
  }
  private filterArrayByCompletedTasksOrNot = (task: TaskWithId, isCompletedtasks: boolean): boolean => {
    let dateInMs: number = new Date(task.task.date).getTime();
    return (task.task.isCompleted === isCompletedtasks) && (this.currentFromDate <= dateInMs) && (dateInMs <= this.currentToDate);
  }


  private createSumPriceObservable(): Observable<number> {
    return this.tasks$.pipe(map(data => this.calculateSumPriceFromArray(data)));
  }

  private updateTasksCounterAndPrice() {
    this.apiService.getTasksValues().subscribe((tasks: TaskWithId[]) => {
      this.tasksSubject.next(tasks.filter(task => this.filterArrayByCompletedTasksOrNot(task, this.isCompleted)));
      this.price$ = this.createSumPriceObservable();
    });
  }

}
