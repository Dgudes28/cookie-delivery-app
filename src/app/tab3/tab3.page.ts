import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
  public fromStringDate: string;
  public toStringDate: string;

  public fromDateInMS: BehaviorSubject<number> = new BehaviorSubject<number>(1);
  public toDateInMS: BehaviorSubject<number> = new BehaviorSubject<number>(1);

  constructor(private datePipe: DatePipe) { }

  ngOnInit(): void {
    let date: Date = new Date(Date.now());
    date.setHours(0, 0, 0, 0);
    this.fromDateInMS.next(date.getTime());
    console.log("form date initialized", date.getTime());
    date.setHours(23, 59, 59, 999);
    this.toDateInMS.next(date.getTime());
    console.log("to date initialized", date);
    // this.fromDate = this.datePipe.transform(date, "yyyy-MM-dd", "+02", "en-GB");
    this.fromDateInMS.subscribe(date => { 
      this.fromStringDate = this.datePipe.transform(date, "yyyy-MM-dd", "+02", "en-GB");
    });
    this.toDateInMS.subscribe(date => {
      this.toStringDate = this.datePipe.transform(date, "yyyy-MM-dd", "+02", "en-GB");
    });
    // console.log("from date" + this.fromDateInMS);
    // console.log("to date" + this.toDateInMS);
  }

  // public fromDateChanged(dateEvent: CustomEvent): void {
  //   let date: Date = new Date(dateEvent.detail.value);
  //   date.setHours(0, 0, 0, 0);
  //   this.fromDateInMS.next(date.getTime());
  //   console.log("form date updated", date.getTime());
  // }

  // public toDateChanged(dateEvent: CustomEvent): void {
  //   let date: Date = new Date(dateEvent.detail.value);
  //   date.setHours(23, 59, 59, 999);
  //   this.toDateInMS.next(date.getTime());
  //   console.log("to date updated", date);
  // }
  public fromDateInMsChanged(dateInMs: number): void {
    this.fromDateInMS.next(dateInMs);
    // console.log("event from date emit with the value ", dateInMs);
  }

  public toDateInMsChanged(dateInMs: number): void {
    this.toDateInMS.next(dateInMs);
    // console.log("event to date emit with the value ", dateInMs);
  }
}
