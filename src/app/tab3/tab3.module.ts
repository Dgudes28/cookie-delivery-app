import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab3Page } from './tab3.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { Tab3PageRoutingModule } from './tab3-routing.module';
import { DailyStatsComponent } from './daily-stats/daily-stats.component';
import { StatsViewComponent } from './daily-stats/stats-view/stats-view.component';
import { SharedModule } from 'src/shared/shared.module';

@NgModule({
  declarations: [
    Tab3Page,
    DailyStatsComponent,
    StatsViewComponent
  ],
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    RouterModule.forChild([{ path: '', component: Tab3Page }]),
    Tab3PageRoutingModule,
    SharedModule
  ]
})
export class Tab3PageModule { }
