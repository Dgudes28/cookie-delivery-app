import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, mergeMap, tap } from 'rxjs/operators';
import { ApiService } from 'src/core/api-service';
import { DialogService } from 'src/core/dialog.service';
import { ToastService } from 'src/core/toast.service';
import { TaskWithId } from 'src/shared/models/taskWithId';
import { TasksListService } from './tasks-list.service';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TasksListComponent implements OnInit {
  @Input() isCompleted$: Observable<boolean>;
  @Input() fromDateInMs$: Observable<number>;
  @Input() toDateInMs$: Observable<number>;
  @Output() tasksListFinishedLoading: EventEmitter<void> = new EventEmitter<void>();

  private fromDateInMs: number;
  private toDateInMS: number;
  private isCompleted: boolean;
  public taskListSubject$: BehaviorSubject<TaskWithId[]> = new BehaviorSubject<TaskWithId[]>([]);
  public taskslist$: Observable<TaskWithId[]> = this.taskListSubject$.asObservable();
  private values: TaskWithId[] = [];

  constructor(private apiService: ApiService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private cd: ChangeDetectorRef,
    private tasksListService: TasksListService) { }

  ngOnInit() {
    console.log("tasks list ng on in it");
    this.values = [];

    this.fromDateInMs$.pipe(tap(time => this.fromDateInMs = time), mergeMap((fromDateInMs: number) => {
      return this.apiService.getTasksValues().pipe(
        map((tasksWithId: TaskWithId[]) => {
          if (this.toDateInMS !== undefined) {
            return tasksWithId.filter(task => this.tasksListService.filterTasksByTimeLimits(this.fromDateInMs, this.toDateInMS, this.isCompleted, task));
          }
        }));
    })).subscribe(values => {
      if (this.toDateInMS !== undefined) {
        this.values = this.tasksListService.sortArrayByDateAndTime(values);
        this.taskListSubject$.next(this.values);
        this.cd.detectChanges();
      }
    });

    this.toDateInMs$.pipe(tap(time => this.toDateInMS = time), mergeMap((toDateInMs: number) => {
      return this.apiService.getTasksValues().pipe(
        map((tasksWithId: TaskWithId[]) => {
          if (this.fromDateInMs !== undefined) {
            return tasksWithId.filter(task => this.tasksListService.filterTasksByTimeLimits(this.fromDateInMs, this.toDateInMS, this.isCompleted, task));
          }
        }));
    })).subscribe(values => {
      if (this.fromDateInMs !== undefined) {
        this.values = this.tasksListService.sortArrayByDateAndTime(values);
        this.taskListSubject$.next(this.values);
        this.cd.detectChanges();
      }
    });

    this.isCompleted$.pipe(tap((isCompleted: boolean) => this.isCompleted = isCompleted), mergeMap(() => {
      return this.apiService.getTasksValues().pipe(
        map((tasksWithId: TaskWithId[]) => {
          debugger
          if (this.toDateInMS !== undefined) {
            return tasksWithId.filter(task => this.tasksListService.filterTasksByTimeLimits(this.fromDateInMs, this.toDateInMS, this.isCompleted, task));
          }
        }));
    })).subscribe(values => {
      debugger
      if (this.toDateInMS !== undefined) {
        this.values = this.tasksListService.sortArrayByDateAndTime(values);
        this.taskListSubject$.next(this.values);
        this.cd.detectChanges();
      }
    });
    this.tasksListFinishedLoading.emit();
  };

  public taskFinished(task: TaskWithId): void {
    this.dialogService.createDialog("בטוח שברצונך להגדיר שהמשלוח הסתיים?", "כן", "לא").subscribe(
      (doAction: boolean) => {
        if (doAction) {
          let message = "המשלוח הושלם";
          this.toastService.createSucessToast(message, 1000);
          this.values.splice(this.values.indexOf(task), 1);
          this.apiService.completeTask(task);
        }
      });
  }

  public deleteTask(task: TaskWithId) {
    this.dialogService.createDialog("בטוח שברצונך למחוק את המשלוח?", "כן", "לא").subscribe(
      (doAction: boolean) => {
        if (doAction) {
          debugger
          this.apiService.deleteTask(task).then(() => {
            let message = "המשלוח נמחק";
            this.toastService.createErrorToast(message, 1000);
          }
          );
        }
      });
  }

  public serachOnWaze(task: TaskWithId): void {
    if (!!task.task.adress) {
      window.location.href = `https://waze.com/ul?q=${task.task.adress}`;
    }
  }
}