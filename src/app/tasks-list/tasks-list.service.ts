import { Injectable } from '@angular/core';
import { TaskWithId } from 'src/shared/models/taskWithId';

@Injectable({
  providedIn: 'root'
})
export class TasksListService {

  constructor() { }

  public sortArrayByDateAndTime(values: TaskWithId[]): TaskWithId[] {
    return values.sort((a, b) => {
      if (Date.parse(a.task.date) !== Date.parse(b.task.date)) {
        if (Date.parse(a.task.date) < Date.parse(b.task.date)) {
          return -1;
        }
        return 1;
      }
      if (this.isSecondStringTimeBigger(a.task.time, b.task.time)) {
        return -1;
      }
      return 1;
    });
  }

  private isSecondStringTimeBigger(firstTime: string, secondTime: string) {
    return this.convertStringTimeToMinutes(firstTime) < this.convertStringTimeToMinutes(secondTime);
  }

  private convertStringTimeToMinutes(time: string): number {
    let timeParts: string[] = time.split(':');
    return parseInt(timeParts[0]) * 60 + parseInt(timeParts[1]);
  }

  public filterTasksByTimeLimits = (minTime: number, maxTime: number, isCompleted: boolean, task: TaskWithId) => {
    let taskTime: number = Date.parse(task.task.date);
    // console.log(`minTime: ${minTime}, maxTime: ${maxTime}, isCompleted:${isCompleted}`);
    return (task.task.isCompleted === isCompleted) && (minTime < taskTime) && (taskTime < maxTime);
  };
}
