import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/core/api-service';
import { ToastService } from 'src/core/toast.service';
import { Task } from 'src/shared/models/taks';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.scss'],
})
export class AddTaskComponent implements OnInit {

  public todayDate: string;
  public timeNow: string;
  public adress: string;
  public price: number;
  public content: string;
  public task: Task;
  constructor(private datePipe: DatePipe, private apiService: ApiService, private toastService: ToastService) { }

  ngOnInit() {
    let msDate: number = Date.now();
    this.todayDate = this.datePipe.transform(msDate, "yyyy-MM-dd", "+02", "en-GB");
    this.timeNow = this.datePipe.transform(msDate, "HH:mm", "+02", "en-GB");
    this.adress = "תל אביב";

  }

  public addTask(): void {
    this.todayDate = this.datePipe.transform(this.todayDate, "yyyy-MM-dd", "+02", "en-GB");
    // this.dateInDateObject = new Date(this.todayDate);
    this.task = new Task(this.todayDate, this.timeNow, this.adress, this.content, this.price);
    this.apiService.getTasksRef().push(this.task).then((value) => {
      let message = "ההזמנה התווספה";
      this.toastService.createSucessToast(message, 1500),
        // console.log(`the request passed with value ${value}`),
        error => {
          debugger
          let message = "אין הרשאות להוספת משלוח";
          this.toastService.createErrorToast(message, 1500);
          console.error(error)
        }
    });
  }

}
