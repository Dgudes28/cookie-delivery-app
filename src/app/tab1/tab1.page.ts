import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  private fromDateInMs: Subject<number> = new Subject();
  private toDateInMs: Subject<number> = new Subject();
  private isCompleted: Subject<boolean> = new Subject();

  public fromDateInMs$: Observable<number> = this.fromDateInMs.asObservable();
  public toDateInMs$: Observable<number> = this.toDateInMs.asObservable();
  public isCompleted$: Observable<boolean> = this.isCompleted.asObservable();

  constructor() { }

  public initializeTasksList(): void {
    let date: Date = new Date(Date.now());
    date.setHours(0, 0, 0, 0);
    this.fromDateInMs.next(date.getTime());
    console.log("form date initialized", date.getTime());
    date.setHours(23, 59, 59, 999);
    this.toDateInMs.next(date.getTime());
    console.log("to date initialized", date);
    this.isCompleted.next(false);
  }

  public tasksModePicked(pickedEvent: CustomEvent): void {
    if (pickedEvent.detail.value === "completed") {
      this.isCompleted.next(true);
    }
    else {
      this.isCompleted.next(false);
    }
  }

  public fromDateInMsChanged(dateInMs: number): void {
    this.fromDateInMs.next(dateInMs);
    console.log("event from date emit with the value ", dateInMs);
  }

  public toDateInMsChanged(dateInMs: number): void {
    this.toDateInMs.next(dateInMs);
    console.log("event to date emit with the value ", dateInMs);
  }
}
