import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { Tab1Page } from './tab1.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';
import { Tab1PageRoutingModule } from './tab1-routing.module';
import { TasksListComponent } from '../tasks-list/tasks-list.component';
import { PrintTaskPipe } from 'src/shared/models/pipes/print-task-pipe';
import { SharedModule } from 'src/shared/shared.module';
import { SignInComponent } from '../sign-in/sign-in.component';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    Tab1PageRoutingModule,
    SharedModule,
    AngularFireModule.initializeApp(environment.firebaseConfig)
  ],
  declarations: [
    Tab1Page,
    TasksListComponent,
    PrintTaskPipe,
    SignInComponent
  ]
})
export class Tab1PageModule { }
