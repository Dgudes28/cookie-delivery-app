import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/core/auth.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent {

  constructor(private authService: AuthService) { }

  public googleAuth(): void {
    this.authService.googleAuth();
  }

  public signIn(mail: string, password: string): void {
    debugger
    this.authService.emailAuth(mail, password);

  }
}
