import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private toastController: ToastController) { }
  createSucessToast(message: string, duration: number): void {
    this.toastController.create({
      color: "primary",
      message: message,
      duration: duration,
      position: "top",
      cssClass: "toast-class",
    }).then(toast => toast.present());
  }

  createErrorToast(message: string, duration: number): void {
    this.toastController.create({
      color: "danger",
      message: message,
      duration: duration,
      position: "top",
      cssClass: "toast-class",
    }).then(toast => toast.present());
  }
}
