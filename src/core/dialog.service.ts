import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(private alertController: AlertController) { }

  public createDialog(message: string, primarySelectionMessage: string, secondarySelectionMessage: string): Subject<boolean> {
    let returnValue: Subject<boolean> = new Subject();
    this.alertController.create({
      header: 'אזהרה',
      message: message,
      buttons: [
        {
          text: secondarySelectionMessage,
          role: 'cancel',
          handler: () => {
            returnValue.next(false);
          }
        }, {
          text: primarySelectionMessage,
          handler: () => {
            returnValue.next(true);
          }
        }
      ]
    }).then(dilaog => dilaog.present());
    return returnValue;
  }
}
