import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import firebase from 'firebase';
import { env } from 'process';
import { environment } from 'src/environments/environment';
import { ToastService } from './toast.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public isLoggedIn: boolean = false;
  constructor(private router: Router, private toastService: ToastService) { }

  googleAuth() {
    return this.authLogin(new firebase.auth.GoogleAuthProvider());
  }

  public emailAuth(mail: string, password: string) {
    debugger
    // return this.authLogin(new firebase.auth.EmailAuthProvider());
    firebase.auth().signInWithEmailAndPassword(mail, password).then(credential => {
      this.isLoggedIn = true;
      this.router.navigateByUrl('tabs/tab1');
    }).catch(() => {
      // if (!credential.user.emailVerified) {
      // firebase.auth().createUserWithEmailAndPassword(mail, password)
      //   .then((user) => {
      //     console.log("the user is created in", user.user.email);
      //     // this.loggedIn = !!sessionStorage.getItem('user');
      //     // Signed in 
      //     // ...
      //   })
      //   .catch((error) => {
      let message = "ארעה שגיאה בהתחברות למשתמש";
      this.toastService.createErrorToast(message, 1500);
      //     // ..
      //   });
      // }
    });
    // firebase.auth().createUserWithEmailAndPassword(mail, password)
    //   .then((user) => {
    //     console.log("the user is logged in", user.user.email);
    //     // this.loggedIn = !!sessionStorage.getItem('user');
    //     // Signed in 
    //     // ...
    //   })
    //   .catch((error) => {
    //     var errorCode = error.code;
    //     var errorMessage = error.message;
    //     // ..
    //   });
  }

  authLogin(provider) {
    firebase.auth().signInWithPopup(provider)
      .then((result: firebase.auth.UserCredential) => {
        debugger
        console.log('You have been successfully logged in!');
      }).catch((error) => {
        console.log(error)
      })
  }
}
