import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { BehaviorSubject } from 'rxjs';
import { Task } from 'src/shared/models/taks';
import { TaskWithId } from 'src/shared/models/taskWithId';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  tasksValues: BehaviorSubject<TaskWithId[]> = new BehaviorSubject<TaskWithId[]>([]);
  values: TaskWithId[] = [];
  constructor(private firestore: AngularFireDatabase) { }

  public getTasksRef() {
    return this.firestore.database.ref("Tasks");
  }

  public deleteTask(task: TaskWithId): Promise<TaskWithId> {
    return this.firestore.database.ref(`Tasks/${task.id}`).remove();
  }

  public getTasksValues(): BehaviorSubject<TaskWithId[]> {
    // if (this.values.length > 0) {
    //   return this.tasksValues.();
    // }
    this.getTasksRef().on("value", snapshots => {
      // console.log(snapshots.numChildren() + "childrens");
      if (this.values.length > 0) {
        this.values = [];
      }

      snapshots.forEach(snapshot => {
        let task: Task = snapshot.val();
        this.values = [...this.values, <TaskWithId>{ id: snapshot.key, task: task }];
      });
      this.tasksValues.next(this.values);
    });
    return this.tasksValues;
    // return this.tasksValues.asObservable().pipe(publishReplay(1), refCount());
  }

  public removeTask(id: string): Promise<TaskWithId> {
    return this.firestore.database.ref(`Tasks/${id}`).remove();
  }

  public completeTask(task: TaskWithId): Promise<TaskWithId> {
    task.task.isCompleted = true;
    return this.firestore.database.ref(`Tasks/${task.id}`).update(task.task);
  }
}
